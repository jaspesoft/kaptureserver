# Kapture Server
Es un servidor escrito totalmente en node.js express framework haciendo uso de TypeScript.
La aplicación está dirigida a servir cada funcionalidad de la plataforma de pago y servicios asociados.

### Instalación

```bash
$ npm install
```

### Correr el servidor
```bash
# desarrollo
$ npm run dev
```


### Crear un usuario mongodb
```bash
$ mongo

$ use admin

db.createUser(
  {
    user: "root",
    pwd: "mongo",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)

use jaspe_sima

db.createUser(
  {
    user: "user_jaspe",
    pwd: "1qaz2wsx",
    roles: [ { role: "readWrite", db: "jaspe_sima" } ]
  }
)
```

### Archivo de configuración para el ambiente de desarrollo

```
{
    "development": {
        "server_port": 23287,
        "domain": "http://95.216.221.178:23287",
        "database": {
            "srv": "localhost",
            "user": "user_jaspe",
            "pass": "1qaz2wsx",
            "port": "27017",
            "db": "jaspe_sima"
        }
    }
}
```

Ésta plataforma ha sido desarrollada por japsesoft c.a y es la reescritura del repositorio original https://gitlab.com/jaspesoft/kapture-server