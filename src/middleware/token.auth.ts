import passport = require('passport');
import bearer = require('passport-http-bearer');
import { adm_clients } from '../apps/settings/models/Clients';
import {getConnection} from "typeorm";

class TokenAuth {
    constructor() {
        this.validateToken();
    }
    private  validateToken() {
        passport.use(new bearer.Strategy(
            function(httpToken: string, cb: any) {
                const user = getConnection()
                            .createQueryBuilder()
                            .from(adm_clients, "client")
                            .where("client.token = :token", { token: httpToken})
                            .getRawOne();
                if (!user) { return cb(null, false); }
                return cb(null, user);
                /*UserModel.findOne({token: httpToken}, (err, user) => {
                    if (err) { return cb(err); }
                    if (!user) { return cb(null, false); }
                    return cb(null, user);
                });*/
            }
        ));
    }
    public AuthMiddleware() {
        return passport.authenticate('bearer', { session: false });
    }
}
const token = new TokenAuth();
export const AuthMiddleware = token.AuthMiddleware();