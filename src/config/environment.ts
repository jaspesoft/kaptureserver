
import dotenv from "dotenv";
import fs from "fs";

if (fs.existsSync(".env")) {
    
    dotenv.config({ path: ".env" });
} else {
    
    dotenv.config({ path: ".env.example" });  // you can delete this after you create your own .env file!
}


const ENVIRONMENT = {
    DOMAIN: process.env["DOMAIN"],
    DEV: process.env["DEV"],
    SOCKET_BLOCKCHAIN: process.env["SOCKET_BLOCKCHAIN"],
    APP_PORT: process.env["APP_PORT"],
};

export default ENVIRONMENT;
