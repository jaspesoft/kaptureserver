import {Entity, PrimaryColumn, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn} from "typeorm";
import { cy_cryptos } from "../../blockchains/models/Cryptos";
import { adm_countries } from "../../settings/models/Countries";
import { Length, IsDate, IsDecimal } from "class-validator";

@Entity()
export class cy_crypto_prices {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("decimal")
    @IsDecimal()
    fiat: number;

    @Column("decimal")
    @IsDecimal()
    usd: number;
    
    @Column("decimal")
    @IsDecimal()
    sat: number;
    
    @Column("decimal")
    @IsDecimal()
    tasa_fiat_usd: number;

    @IsDate()
    @Column('timestamp with time zone')
    date: Date;

    @ManyToOne(type => adm_countries)
    @JoinColumn({ name: "country_id" })
    country_id: adm_countries;

    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;

    /*@ManyToOne(type => Cryptos, cryptos => cryptos.crypto_prices)
    crypto_id = Cryptos;*/


}