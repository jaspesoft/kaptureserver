import { getConnection, getManager } from "typeorm";
import { cy_cryptos} from "../blockchains/models/Cryptos";
import { cy_crypto_prices } from "../prices/models/Crypto_prices";
import { adm_markets_countries } from "../settings/models/Markets_countries";
import { adm_countries } from "../settings/models/Countries";
import { validate } from "class-validator";
import "reflect-metadata";
import axios from "axios";


class PricesService{
    private urlBTC = 'https://yobit.net/api/3/ticker/btc_usd/';

    private async getAllCoin() {
        return await getConnection()
                        .createQueryBuilder()
                        .from(cy_cryptos, "crypto")
                        .getRawMany();
    }

    private async getMarketskapturela() {
        return await getConnection()
        .createQueryBuilder()
        .innerJoin(adm_countries, "adm_countries", "adm_countries.code_country = adm_markets_countries.country_id")
        .from(adm_markets_countries, "")
        .getRawMany();
    
    }

    private async crex24(response: any): Promise<any> {
        const respBTC = await axios.get(this.urlBTC);        
        for (const iterator of response.data) {
            return {
                last: Number(iterator.last).toFixed(8),
                vol: Number(iterator.VolumeInBtc).toFixed(8),
                BTC : {
                    last: respBTC.data.btc_usd.last,
                },
            };
        }
        
    }
    private async graviex(response: any): Promise<any> {
        const respBTC = await axios.get(this.urlBTC);

        return {
            last: Number(response.data.ticker.last).toFixed(8),
            vol: Number(response.data.ticker.vol).toFixed(8),
            BTC : {
                last: respBTC.data.btc_usd.last,
            },
        };
    }

    private async tradesatoshi(response: any): Promise<any> {
        const respBTC = await axios.get(this.urlBTC);

        return {
            last: Number(response.data.result.last).toFixed(8),
            vol: Number(response.data.result.volume).toFixed(8),
            BTC : {
                last: respBTC.data.btc_usd.last,
            },
        };
    }

    private async coinexchange(response: any): Promise<any> {
        const respBTC = await axios.get(this.urlBTC);
        //console.log(respBTC.data);
        return {
            last: Number(response.data.result.LastPrice).toFixed(8),
            vol: Number(response.data.result.Volume).toFixed(8),
            BTC : {
                last: respBTC.data.btc_usd.last,
            },
        };
    }

    private async yobit(response: any, crypto: string): Promise<any> {
        const respBTC = await axios.get(this.urlBTC);
        let last_btc: any;
        let last_usd: any;
        let vol: any;

        switch(crypto) {
            case 'BTC':
                last_btc = 1.0;
                last_usd = respBTC.data.btc_usd.last;
                vol = Number(respBTC.data.btc_usd.vol).toFixed(8);
                break;
            case 'ONX':
                last_btc = Number(response.data.onx_btc.last).toFixed(8);
                last_usd = respBTC.data.btc_usd.last;
                vol = Number(response.data.onx_btc.vol).toFixed(8);
                break;
        }

        return {
            last: last_btc,
            vol: vol,
            BTC : {
                last: last_usd,
            },
        };
    }

    private async getPriceExchanges(urlExchange: string, crypto: string): Promise <any> {
        try {
            const response = await axios.get(urlExchange);
            let new_price: any = [];
            if (/\yobit/g.test(urlExchange)) {
                await this.yobit(response, crypto).then( resp => {
                    new_price = resp;
                });
            } else if (/crex24/.test(urlExchange)) {
                await this.crex24(response).then( resp => {
                    new_price = resp;
                });
            } else if (/graviex/.test(urlExchange)) {
                await this.graviex(response).then( resp => {
                    new_price = resp;
                });

            } else if (/tradesatoshi/.test(urlExchange)) {
                await this.tradesatoshi(response).then( resp => {
                    new_price = resp;
                });
            } else if (/coinexchange/.test(urlExchange)) {
                await this.coinexchange(response).then( resp => {
                    new_price = resp;
                });

            }

            return new_price;

        }catch (e) {
            console.log('error consultado exchange: ' + urlExchange);
            console.log(e);
        }
    }

    private async getLocalPrice(data: any, base: any, country: any, BTCExchange: number) {
        // sacar el promedio de los 3 promedios que retorna localbitcoin
        const avg = Number(data.avg_12h) + Number(data.avg_1h) + Number(data.avg_6h);
        const btcLocalPrice = avg / 3;
        let usd_factor: number;
        if (country.code_currency === 'USD') {
            usd_factor = Number((btcLocalPrice * base.last));
        } else {
            usd_factor = Number(base.last * BTCExchange);
        }
        return {
            code_country: country.code_country,
            country_id: country.id,
            country: country.name_country,
            local_currency: country.code_currency,
            fiat:  Number(btcLocalPrice * base.last).toFixed(6),
            usd: usd_factor.toFixed(6),
            sat: base.last,
            tasa_fiat_usd:  Number(btcLocalPrice / BTCExchange).toFixed(6),
            BTC : {
                price_exchange: BTCExchange,
                local_fiat: Number(btcLocalPrice).toFixed(3),
            },
        };
    }

    private async getPriceForCountries(countries: adm_markets_countries[], base_price: any, localbitcoin: any) {
        const prices = [];
        for (const country of countries) {
            if (base_price.BTC !== undefined) {
                const localPrice = await this.getLocalPrice(localbitcoin[country.code_currency], base_price, country, base_price.BTC.last);
                prices.push(localPrice);
            }
            
        }
        return prices;
    }
    public async taskPrices() {
        const crytos = await this.getAllCoin();
        const markets = await this.getMarketskapturela();

        axios.get('https://localbitcoins.com/bitcoinaverage/ticker-all-currencies/').then(localbitcoin => {
            (async () => {
                for (const crypto of crytos) {
                    const pricesExchange = await this.getPriceExchanges(crypto.endpoint_prices, crypto.symbol);
                    const localPrices = await this.getPriceForCountries(markets, pricesExchange, localbitcoin.data);
                    
                    for (const prices of localPrices) {
                        let new_prices = new cy_crypto_prices();

                        new_prices.fiat = Number(prices.fiat);
                        new_prices.usd = Number(prices.usd);
                        new_prices.sat = prices.sat;
                        new_prices.tasa_fiat_usd = Number(prices.tasa_fiat_usd);
                        new_prices.date = new Date();
                        new_prices.crypto_id = crypto.symbol;
                        new_prices.country_id = prices.code_country;
                        await validate(new_prices);
                        await getManager().save(new_prices);
                    }
                    
                }
            })();
        });
    }
}

export const pricesService = new PricesService();