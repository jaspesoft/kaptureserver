import { Router, Request, Response } from "express";
import { getConnection } from "typeorm";
import { HttpStatus } from "../../lib/http-status";

export class PricesController {
    public routers() {
        const api = Router();
        api.get('/:crypto/:country', (req: Request, res: Response) => {
            this.getPrices(res, req.params.crypto, req.params.country, req.params.currency);
        });

        return api;
    }

    private async getPrices(res: Response, crypto: string, country: string, currency: string) {
        const data = await getConnection().createQueryBuilder()
                    .from('cy_crypto_prices', '')
                    .where('country_id = :country_id and crypto_id = :crypto_id', {
                        country_id: country, crypto_id: crypto
                    }).orderBy('date', 'DESC').getRawOne();

        return res.status(HttpStatus.OK).json({
            usd: data.usd,
            fiat: data.fiat,
            sat: data.sat,
            date: data.date,
            
        });
    }
}
