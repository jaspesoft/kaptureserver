import { Router, Request, Response } from "express";
import { WalletsService } from "./wallets.service";
import { AuthMiddleware } from "../../middleware/token.auth";
import { wal_wallets } from "./models/Wallets";
import { HttpStatus } from "../../lib/http-status";
import { serviceMail } from "../../shared/services/mail.service";

export class WalletsController extends WalletsService {
    public routers() {
        const api = Router();

        api.post('/create/:crypto', AuthMiddleware,
            (req: Request, res: Response) => this.setCreateWallet(req.body, req.params.crypto, res)
        );

        api.get('/mywallets', AuthMiddleware, (req: Request, res: Response) => this.myWallets(req.body.account, res));

        api.post('/history/:crypto', AuthMiddleware,
            (req: Request, res: Response) => this.history(req.body.account, req.params.crypto, res)
        );

        api.post('/password-withdraw', AuthMiddleware,
            (req: Request, res: Response) => this.setPassword(req.body, res)
        );

        api.post('/change-password-withdraw', AuthMiddleware,
            (req: Request, res: Response) => this.setChangePassword(req.body, res)
        );

        api.get('/address/:crypto', AuthMiddleware, (req: Request, res: Response) => {
            this.getAdress({
                crypto_id: req.params.crypto,
                account_id: req.body.account
            }, res);
        });

        api.post('/withdraw/request', AuthMiddleware, (req: Request, res: Response) => this.requestWithdraw(req.body, res));

        api.post('/withdraw/execute', AuthMiddleware, (req: Request, res: Response) => this.executeWithdraw(req.body, res));

        return api;
    }

    private async executeWithdraw(body: any, res: Response) {

    }

    private async getAdress(data: any, res: Response) {

        // se valida que la cuenta no tenga una billetera creada para la crypto correspondiente
        if (!await this.existsWallet(data)) {
            return res.status(HttpStatus.FORBIDDEN).json({
                message: 'Your account does not have a wallet ' + data.crypto_id,
                state: 'faild'
            });
        }

        const data_address = await this.getAddressPayments(data.crypto_id, data.account_id);
        return res.status(HttpStatus.OK).json(data_address);

    }

    private async requestWithdraw(body: any, res: Response) {
        if (!await this.existsCoin(body.coin)) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                message: 'Coin not support',
                state: 'faild'
            });
        }
        await serviceMail.send('welcome', 'angelito_contigo_758@hotmail.com', 'Welcome to Kapture.la', {
            code: 'sss',
            domain: 'asss'
        });

    }

    private async setPassword(body: any, res: Response) {
        this.setCreatePasswordWithdrawal(body.account, body.pass).then(() => {
            return res.status(HttpStatus.OK).json({
                message: 'Password set successfully',
                state: 'ok'
            });
        }).catch(() => {
            return res.status(HttpStatus.BAD_REQUEST).json({
                message: 'Sorry service temporarily disabled',
                state: 'faild'
            });
        });
    }

    private async setChangePassword(body: any, res: Response) {
        this.setChangePasswordWithdrawal(body.account, body.pass, body.current_pass).then((response) => {
            return res.status(HttpStatus.OK).json(response);
        }).catch(err => {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        });
    }

    private async history(account: string, crypto: string, res: Response) {
        this.getHistoryTransacction(account, crypto).then(resp => {
            return res.status(HttpStatus.OK).json(resp);
        }).catch(err => {
            return res.status(HttpStatus.OK).json(err);
        });
    }

    private async myWallets(account: string, res: Response) {
        if (!await this.existsAccount(account)) {
            return res.status(HttpStatus.NOT_FOUND).json({
                message: 'The account number supplied does not exist on the platform',
                state: 'failed',
            });
        }

        const data = await this.getMyWallets(account);
        return res.status(HttpStatus.OK).json(data);
    }

    private async validateAccountAndCoin(account: any, crypto: string, res: Response) {
        if (!await this.existsAccount(account)) {
            return res.status(HttpStatus.NOT_FOUND).json({
                message: 'The account number supplied does not exist on the platform',
                state: 'failed',
            });
        }
        if (!await this.existsCoin(crypto)) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                status: 'faild',
                message: 'Coin not support',
            });
        }
        return true;
    }

    private async setCreateWallet(createWallet: wal_wallets, crypto: string, res: Response) {
        await this.validateAccountAndCoin(createWallet.account_id, crypto, res);

        // se valida que la cuenta no tenga una billetera creada para la crypto correspondiente
        if (await this.existsWallet(createWallet)) {
            return res.status(HttpStatus.EXPECTATION_FAILED).json({
                message: 'The account number already has a bielletera for the cryptocurrency ' + crypto,
                state: 'faild',
            });
        }

        this.setMakeWallet(createWallet, crypto).then(x => {
            if (x.state === 'faild') {
                return res.status(HttpStatus.EXPECTATION_FAILED).json(x);
            } else {
                return res.status(HttpStatus.OK).json({
                    message: 'The wallet was created successfully, we invite you to make a deposit',
                    state: 'ok',
                });
            }
        });

    }
}
