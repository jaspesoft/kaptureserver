import { cy_cryptos } from "../blockchains/models/Cryptos";
import { getConnection, getManager } from "typeorm";
import { wal_accounts } from "./models/Accounts";
import { wal_wallets } from "./models/Wallets";
import { WalletTransaction } from "./wallet.transaction";
import { validate } from "class-validator";
import { cy_crypto_prices } from "../prices/models/Crypto_prices";
import { wal_transactions } from "./models/Transactions";
import { wal_settings_account } from "./models/Settings_account";
import { Tools } from "../../lib/tools";
import { wal_unused_address } from "./models/Unuset_addresses";

export class WalletsService extends WalletTransaction {

    public async existsAccount(accountID: string): Promise<boolean> {
        const account = await getConnection().getTreeRepository(wal_accounts)
            .findAndCount({where: {id: accountID}});

        return account.length > 0;
    }

    public async existsWallet(data: wal_wallets): Promise<boolean> {
        const data_wallet = await getConnection().getRepository(wal_wallets)
            .findAndCount({where: {crypto_id: data.crypto_id, account_id: data.account_id}});

        return data_wallet.length > 0;
    }

    public async setMakeWallet(data: wal_wallets, crypto: any): Promise<any> {
        // buscar la cuenta para tener los seed's
        const data_account = await getConnection().getTreeRepository(wal_accounts)
            .findOne({where: {id: data.account_id}});

        if (!data_account) {
            return {
                message: 'the account code does not exist',
                state: 'faild',
            };
        }
        const seed = data_account.seed;
        const wallet = await this.walletProvider.getWallet(crypto, seed);

        let _w = new wal_wallets();
        _w.account_id = data.account_id;
        _w.address = wallet.address;
        _w.balance = 0;
        _w.child = 0;
        _w.crypto_id = crypto;
        _w.testnet = wallet.testnet;
        _w.change = false;

        try {
            await validate(_w);
            await getManager().save(_w);

            return {
                message: 'The wallet was created successfully, we invite you to make a deposit',
                state: 'ok',
            }
        } catch (err) {
            return {
                message: 'Sorry, a service failure was detected at the time of generating the wallet.',
                state: 'faild',
            };
        }
    }

    public async existsCoin(coin: string): Promise<boolean> {
        const data = await getConnection().getRepository(cy_cryptos)
            .findAndCount({where: {crypto: coin}});

        return data.length > 0;
    }

    public async getMyWallets(account: string): Promise<any[]> {
        return await getConnection().createQueryBuilder()
            .select('DISTINCT ON (cy_crypto_prices.crypto_id) cy_crypto_prices.crypto_id', 'crypto_id')
            .addSelect("name_crypto", "")
            .addSelect("fiat", "")
            .addSelect("usd", "")
            .addSelect("sat", "")
            .addSelect("balance", "")
            .addSelect("address")
            .addSelect("cy_crypto_prices.crypto_id", "crypto_id")
            .innerJoin(cy_crypto_prices, "cy_crypto_prices", "cy_crypto_prices.crypto_id = wal_wallets.crypto_id")
            .innerJoin(cy_cryptos, "cy_cryptos", "cy_cryptos.symbol = wal_wallets.crypto_id")
            .from(wal_wallets, "")
            .where("wal_wallets.account_id = :accountId", {accountId: account})
            .orderBy('cy_crypto_prices.crypto_id')
            .addOrderBy('date', 'DESC')
            .getRawMany();

    }

    public async getHistoryTransacction(account: string, crypto: string): Promise<any> {
        let data = getConnection().createQueryBuilder()
            .select("wal_transactions.crypto_id", "crypto")
            .addSelect("wal_transactions.tx", "tx")
            .addSelect("wal_transactions.address", "address")
            .addSelect("wal_transactions.type_transaction", "type_transaction")
            .addSelect("wal_transactions.amount", "amount")
            .addSelect("wal_transactions.confirmed", "confirmed")
            .addSelect("wal_transactions.date", "date")
            .innerJoin(wal_wallets, "wal_wallets", "wal_wallets.id = wal_transactions.wallet_id")
            .from(wal_transactions, "wal_transactions")
            .where("wal_wallets.account_id = :accountId", {accountId: account})
            .andWhere("wal_transactions.change = false")
            .orderBy("wal_transactions.date", "DESC");

        if (crypto === 'all') {
            return await data.getRawMany();
        } else {
            return await data.andWhere("wal_transactions.crypto_id = :cryptoId", {cryptoId: crypto})
                .getRawMany();
        }
    }

    public async setChangePasswordWithdrawal(accountID: string, pass: string, current_pass: string): Promise<any> {
        // validar la clave actual
        const data = await getConnection().createQueryBuilder()
            .from(wal_settings_account, "")
            .where("account_id = :account", {account: accountID}).getRawOne();
        if (!data) {
            return {
                state: 'faild',
                message: 'Account not register in the plataform',
            };
        }

        // validar la clave actual
        if (!await Tools.comparePassword(current_pass, data.password_withdraw)) {
            return {
                state: 'faild',
                message: 'Incorrect current password, verify and try again please',
            };
        }
        try {
            await getConnection().createQueryBuilder()
                .update(wal_settings_account)
                .set({
                    password_withdraw: await Tools.makePassword(pass)
                }).where("account_id = :accountId", {accountId: accountID}).execute();
            return {
                state: 'ok',
                message: 'Change password successful'
            }
        } catch (e) {
            return {
                state: 'faild',
                message: 'Error internal'
            }
        }
    }

    public async setCreatePasswordWithdrawal(account: string, pass: string): Promise<boolean> {
        try {
            await getConnection().createQueryBuilder()
                .update(wal_settings_account)
                .set({
                    password_withdraw: await Tools.makePassword(pass)
                }).where("account_id = :accountId", {accountId: account}).execute();
            return true;
        } catch (e) {
            return false;
        }
    }

    public async getAddressPayments(crypto: string, account: string): Promise<any> {
        const wallet = await getConnection().getRepository(wal_wallets)
            .findOne({where: {crypto_id: crypto, account_id: account}});

        const data_account = await getConnection().getRepository(wal_accounts)
            .findOne({where: {id: account}});

        const childWallet = wallet.child + 1;

        const wallet_blockchain = await this.walletProvider.getWallet(
            crypto, data_account.seed, String(childWallet)
        );

        const data_crypt = await getConnection().getRepository(cy_cryptos)
            .findOne({where: {symbol: crypto}});

        const unset = new wal_unused_address();
        unset.address = wallet_blockchain.address;
        unset.child = childWallet;
        unset.change = false;
        unset.wallet_id = wallet;
        unset.crypto_id = data_crypt;
        await getConnection().manager.save(unset);

        return {
            adddress: wallet_blockchain.address,
            qr: await wallet_blockchain.qr
        };
    }
}
