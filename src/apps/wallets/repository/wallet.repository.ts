import { IRepository } from "../../../architect/interface/repository.interface";
import { dbconnection, socketBlockchain } from "../../../server";
import { wal_wallets } from "../models/Wallets";
import { Repository } from "typeorm";
import { injectable } from "inversify";
import { wal_unspent_addresses } from "../models/Unspent_addresses";
import { RepositoryAbstract } from "../../../architect/abstract/repository.abstract";

@injectable()
export class WalletRepository extends RepositoryAbstract {

    getClassRepository(): Repository<wal_wallets> {
        return dbconnection.getRepository(wal_wallets);
    }

    async findWallet(paramsFilter: object): Promise<wal_wallets> {
        return await this.getClassRepository().findOne({where: paramsFilter});
    }

    async updateBalanceByWallet(wallet: wal_wallets) {
        // suma de todas las direcciones no gastadas para actualizar el balance
        const new_balance = await dbconnection.createQueryBuilder()
            .select("COALESCE( SUM(wal_unspent_addresses.amount), 0)", "sum")
            .from(wal_unspent_addresses, "wal_unspent_addresses")
            .where("wal_unspent_addresses.is_spent = :spent", { spent: false })
            .andWhere("wal_unspent_addresses.wallet_id = :wallet", { wallet: wallet.id })
            .getRawOne();

        const balance = Number(parseFloat(new_balance.sum).toFixed(8));
        socketBlockchain.notifySocketBalance(wallet.account_id.id, balance, wallet.crypto_id.symbol);

        await this.getClassRepository().update(wallet.id, {balance: balance});
    }

}
