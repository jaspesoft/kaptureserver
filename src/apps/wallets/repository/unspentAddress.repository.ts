import { injectable } from "inversify";
import { IRepository } from "../../../architect/interface/repository.interface";
import { Repository } from "typeorm";
import { dbconnection } from "../../../server";
import { wal_unspent_addresses } from "../models/Unspent_addresses";
import { wal_wallets } from "../models/Wallets";
import { RepositoryAbstract } from "../../../architect/abstract/repository.abstract";

@injectable()
export class UnspentAddressRepository extends RepositoryAbstract{

    getClassRepository(): Repository<wal_unspent_addresses> {
        return dbconnection.getRepository(wal_unspent_addresses);
    }

    async findUnspentAddressByWallet(wallet: wal_wallets): Promise<wal_unspent_addresses[]> {
        return await this.getClassRepository().find({where: {wallet_id: wallet}});
    }

}
