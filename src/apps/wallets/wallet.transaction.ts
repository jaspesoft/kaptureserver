import { WalletProvider } from "../../shared/providers/wallet.provider";
import { BlockchainServices } from "../../shared/services/blockchain.services";
import { TransactionBuilder, ECPair } from "bitcoinjs-lib";
import { dbconnection } from "../../server";
import { wal_wallets } from "./models/Wallets";
import { wal_withdrawal_request } from "./models/Withdrawl";
import { wal_transactions } from "./models/Transactions";
import { wal_unspent_addresses } from "./models/Unspent_addresses";
import { WalletRepository } from "./repository/wallet.repository";
import { inject } from "inversify";
import { UnspentAddressRepository } from "./repository/unspentAddress.repository";
import { TYPES } from "../../architect/types";

let sb = require("satoshi-bitcoin");
let coinSelect = require("coinselect");

export class WalletTransaction {
    public walletProvider = new WalletProvider();
    private usedChangeChild = false;
    private walletRepository: WalletRepository;
    private unspentAddressRepository: UnspentAddressRepository;

    constructor(
        @inject(TYPES.Wallet) walletRepository: WalletRepository,
        @inject(TYPES.Unspent) unspentAddressRepository: UnspentAddressRepository,
    ) {
        this.walletRepository = walletRepository;
        this.unspentAddressRepository = unspentAddressRepository;
    }

    async setCreateTransaction(withdrawal: wal_withdrawal_request): Promise<any> {

        const walletRepository = dbconnection.getRepository(wal_wallets);
        const wallet = await this.walletRepository.findWallet({
            account: withdrawal.account_id,
            coin: withdrawal.crypto_id
        });

        // const seed = WalletProvider.getDescrypt(account.seed) + account.created_at;
        const seed = withdrawal.account_id.seed;

        const transaction = await this.createTransaction(withdrawal.amount, withdrawal.address, seed, wallet);
        if (transaction.state === 'faild') {
            return transaction;
        }

        const blockchain = await new BlockchainServices(wallet.crypto_id.symbol);
        const connection = await blockchain.getConnection();
        try {
            const txid = await connection.sendRawTransaction(transaction.hex);
            let txinfo = null;
            // tslint:disable-next-line:variable-name
            const _tx = await connection.getrawtransaction(txid);
            txinfo = await connection.decoderawtransaction(_tx);

            if (txinfo === null) {
                console.log('TX reject');
                return;
            }

            // se recorre txu que son los tx que no habian sido gastados y que ahora se gastaron y
            // seran eliminados de la tabla de transacciones no gastadas
            for (const tx of transaction.txu) {
                await this.unspentAddressRepository.delete({tx: tx.tx});
            }

            // actualizar solicitud de retiro
            await dbconnection.getRepository(wal_withdrawal_request).update(withdrawal.id, {executed: true});

            // transaction.address es verdadero si se uso una address de cambio
            if (this.usedChangeChild) {
                for (const vout of txinfo.vout) {
                    if (vout.scriptPubKey.addresses[0] === transaction.address) {
                        const unspe = new wal_unspent_addresses();
                        unspe.address = transaction.address;
                        unspe.child = transaction.child;
                        unspe.amount = transaction.amount;
                        unspe.wallet_id = wallet;
                        unspe.address = transaction.address;
                        unspe.tx = txid;
                        unspe.txindx = vout.n;
                        unspe.change = true;
                        await this.unspentAddressRepository.save(unspe);
                    }
                }
                await this.walletRepository.updateOne(wallet.id, {child: (transaction.child + 1)});
            }

            // registrar la transaccion de retiro
            const newtx = new wal_transactions();
            newtx.address = withdrawal.address;
            newtx.amount = withdrawal.amount;
            newtx.crypto_id = withdrawal.crypto_id;
            newtx.tx = txid;
            newtx.type_transaction = 'R';
            newtx.wallet_id = wallet;
            newtx.date = new Date();

            await dbconnection.getRepository(wal_transactions).save(newtx);

            await this.walletRepository.updateBalanceByWallet(wallet);

            return {
                message: 'The retreat was successful', state: 'ok'
            };
        } catch (error) {
            console.log(transaction.hex);

            return {
                state: 'faild',
                message: 'Error internal',
            };
        }

    }

    private async createTransaction(
        amount: number,
        addressPayment: string,
        seed: string,
        wallet: wal_wallets): Promise<any> {

        let changeAddress = '';
        let feeRate = 300; // satoshis per byte


        const tx = new TransactionBuilder(this.walletProvider.crypto_network.getNetworkParams(wallet.crypto_id.symbol));
        tx.setVersion(1);

        // busco todos los tx no gastados
        const txUnspe = await this.unspentAddressRepository.findUnspentAddressByWallet(wallet);
        let balance = 0;

        const txused = [];
        let w: any;
        for (const unspe of txUnspe) {
            if (balance > amount) {
                break;
            }
            w = await this.walletProvider.getWallet(wallet.crypto_id.symbol, seed, String(unspe.child), unspe.change);
            console.log(w)
            // ingreso los tx no gastados que se usaran para enviar las monedas en un arreglo para su posterior uso
            txused.push({
                tx: unspe.tx,
                indx: unspe.txindx,
                amount: unspe.amount,
                prv: w.prv,
            });
            balance = balance + unspe.amount;
        }
        balance = Number(parseFloat(String(balance)).toFixed(8));

        // agrego las entrada a la construccion de la nueva transaccion
        for (const dat of txused) {
            tx.addInput(dat.tx, dat.indx);
        }
        balance = sb.toSatoshi(balance);
        const sendAmount = sb.toSatoshi(amount);

        tx.addOutput(addressPayment, sendAmount);

        // calcular el fee de red

        const {inp_tx, out_tx, fee} = coinSelect(txused, [{address: addressPayment, value: sendAmount}], feeRate);
        console.log(sb.toBitcoin(fee));
        // se calculo si hay unas monedas sobrantes
        let surplus = balance - (sendAmount + fee);

        //surplus = Number(parseFloat(String(surplus)).toFixed(8));
        if (surplus < 0) {
            return {
                state: 'faild',
                message: 'Insufficient balance',
            };
        } else if (surplus > 0.00000000) {
            w = await this.walletProvider.getWallet(wallet.crypto_id.symbol, seed, String(wallet.change), true);
            changeAddress = w.address;
            tx.addOutput(w.address, surplus);
            this.usedChangeChild = true;
        }

        // firmar las salidas
        let indx = 0;
        for (const dat of txused) {
            const keyPair = await ECPair.fromWIF(dat.prv, this.walletProvider.crypto_network.getNetworkParams(wallet.crypto_id.symbol));
            tx.sign(indx, keyPair);

            indx++;
        }
        //console.log(tx.build().toHex());
        if (this.usedChangeChild) {
            return {
                address: changeAddress,
                child: wallet.change,
                amount: sb.toBitcoin(surplus),
                hex: tx.build().toHex(),
                txu: txused,
                state: 'ok',
            };
        } else {
            return {
                hex: tx.build().toHex(),
                txu: txused,
                state: 'ok',
            };
        }
    }

}
