import { ManyToOne, JoinColumn, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { wal_wallets } from "./Wallets";
import { IsBoolean, MaxLength } from "class-validator";

@Entity()
export class wal_unspent_addresses {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("numeric")
    amount: number;

    @Column("character varying")
    @MaxLength(100)
    address: string;

    @Column("integer")
    child: number;

    @Column("integer")
    txindx: number;

    @Column("character varying")
    @MaxLength(200)
    tx: string;

    @IsBoolean()
    @Column({ type: "boolean", default: false })
    change: boolean;

    @IsBoolean()
    @Column({ type: "boolean", default: false })
    is_spent: boolean;
    
    @ManyToOne(type => wal_wallets)
    @JoinColumn({ name: "wallet_id" })
    wallet_id: wal_wallets;
}
