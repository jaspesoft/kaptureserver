import { ManyToOne, JoinColumn, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { wal_accounts } from "./Accounts";

@Entity()
export class wal_settings_account {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "boolean",
        default: true
    })
    notify_email: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    notify_sms: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    two_factor: boolean;

    @Column("character varying")
    password_withdraw: string;

    @ManyToOne(type => wal_accounts)
    @JoinColumn({ name: "account_id" })
    account_id: wal_accounts
}   