import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from "typeorm";
import { IsBoolean, IsDate, IsDecimal, MaxLength } from "class-validator";
import { cy_cryptos } from "../../blockchains/models/Cryptos";
import { wal_accounts } from "./Accounts";

@Entity()
export class wal_withdrawal_request {
    @PrimaryColumn()
    id: number;

    @IsDecimal()
    @Column("numeric")
    amount: number;

    @Column("character varying")
    @MaxLength(100)
    address: string;

    @Column("character varying")
    @MaxLength(8)
    validation_code: string;

    @IsDate()
    @Column('timestamp with time zone')
    expires_at: Date;

    @IsBoolean()
    @Column({ type: "boolean", default: false })
    expired: boolean;

    @IsBoolean()
    @Column({ type: "boolean", default: false })
    executed: boolean;

    @OneToOne(type => wal_accounts)
    @JoinColumn({ name: "account_id" })
    account_id: wal_accounts;

    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;


}
