import { ManyToOne, JoinColumn, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { adm_clients } from "../../settings/models/Clients";

@Entity()
export class wal_accounts {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column("character varying")
    seed: string;

    @ManyToOne(type => adm_clients)
    @JoinColumn({ name: "client_id" })
    client_id: adm_clients;
}