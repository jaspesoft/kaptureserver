import { ManyToOne, JoinColumn, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { wal_wallets } from "./Wallets";
import { IsBoolean, MaxLength } from "class-validator";
import { cy_cryptos } from "../../blockchains/models/Cryptos";

@Entity()
export class wal_unused_address {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("character varying")
    @MaxLength(100)
    address: string;

    @Column("integer")
    child: number;

    @IsBoolean()
    @Column({ type: "boolean", default: false })
    change: boolean;

    @ManyToOne(type => wal_wallets)
    @JoinColumn({ name: "wallet_id" })
    wallet_id: wal_wallets;

    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;
}
