import { PrimaryColumn, JoinColumn, Column, Entity, OneToOne } from "typeorm";
import { IsDecimal, MaxLength, IsInt } from "class-validator";
import { wal_accounts } from "./Accounts";
import { cy_cryptos } from "../../blockchains/models/Cryptos";

@Entity()
export class wal_wallets {
    @PrimaryColumn()
    id: number;

    @IsDecimal()
    @Column("numeric")
    balance: number;

    @IsInt()
    @Column("integer")
    child: number;

    @Column({
        type: "boolean",
        default: false,
    })
    testnet: boolean;

    @Column("character varying")
    @MaxLength(100)
    address: string;

    @Column({
        type: "boolean",
        default: false,
    })
    change: boolean;

    @OneToOne(type => wal_accounts)
    @JoinColumn({ name: "account_id" })
    account_id: wal_accounts;

    
    @OneToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;
}
