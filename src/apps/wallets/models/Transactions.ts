import { cy_cryptos } from "../../blockchains/models/Cryptos";
import { ManyToOne, JoinColumn, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { wal_wallets } from "./Wallets";
import { MaxLength, IsDate, IsInt } from "class-validator";

@Entity()
export class wal_transactions {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("character varying")
    @MaxLength(100)
    address: string;
    
    @Column("numeric")
    amount: number;

    @Column("character varying")
    @MaxLength(200)
    tx: string;

    @Column("character varying")
    @MaxLength(1)
    type_transaction: string;
    
    @IsDate()
    @Column('timestamp with time zone')
    date: Date;

    @Column("boolean")
    confirmed: boolean;

    @Column("boolean")
    change: boolean;

    @IsInt()
    @Column("integer")
    child: number;
    
    @ManyToOne(type => wal_wallets)
    @JoinColumn({ name: "wallet_id" })
    wallet_id: wal_wallets;

    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;
}