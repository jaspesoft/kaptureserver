import { adm_clients } from "./models/Clients";
import { wal_accounts } from "../wallets/models/Accounts";
import { wal_settings_account } from "../wallets/models/Settings_account";
import { getConnection, getManager, PromiseUtils } from "typeorm";
import { Tools } from "../../lib/tools";
import { validate } from "class-validator";
import { WalletProvider } from "../../shared/providers/wallet.provider";
import { Guid } from "guid-typescript";
import { adm_countries } from "./models/Countries";
import { AuthLogin } from "./auth.interface";
import { serviceMail } from "../../shared/services/mail.service";
import ENVIRONMENT from "../../config/environment";



export class SettingsService {
    public library = new Tools();

    public async getCountries(): Promise<any> {
      return getConnection().createQueryBuilder()
                        .from(adm_countries, "").getRawMany();
    }

    public async validateExistsEmail(userEmail: string): Promise<any> {
        const data_user = await getConnection().createQueryBuilder()
                            .from(adm_clients, "adm_clients")
                            .where("adm_clients.email = :email", {email: userEmail}).getRawOne();
        
        if (data_user) {
          return {
            message: 'Email is already registered',
            state: 'faild',
          };
        } else {
          return {
            state: 'ok',
          };
        }
    }
    
    public async setCreateUser(user: adm_clients) {
        await getManager().transaction(async transactionalEntityManager => {
            // crear el usuario
            const newUser = new adm_clients();
            newUser.email = user.email;
            newUser.country_id = user.country_id;
            newUser.password = await Tools.makePassword(user.password);
            newUser.is_business = false;
            newUser.is_active = false;
            newUser.date_joined = new Date();
            newUser.token = await this.library.getRandom(6);
            // console.log(newUser);
            await validate(newUser);
            const insertUserResult = await transactionalEntityManager.save(newUser);
            
            
            // crear la cuenta del usuario
            const _seed = WalletProvider.getCreateMnemonic();
            const uuid = String(Guid.create());
            const newAccount = new wal_accounts();
            newAccount.client_id = insertUserResult;
            newAccount.id = uuid;
            newAccount.seed = _seed;

            await validate(newAccount);
            const insertAccountResult = await transactionalEntityManager.save(newAccount);

            const newSettingAccount = new wal_settings_account();
            newSettingAccount.account_id = insertAccountResult;
            newSettingAccount.notify_email = true;
            newSettingAccount.notify_sms = false;
            newSettingAccount.two_factor = false;
            await transactionalEntityManager.save(newSettingAccount);

            
        });
       
    }

    public async makeLogin(dataLogin: AuthLogin): Promise<any> {
      const user = await getConnection().createQueryBuilder()
                    .select(["adm_clients.*", "wal_accounts.id as account"])
                    .from(adm_clients, "")
                    .innerJoin(wal_accounts, "wal_accounts", "wal_accounts.client_id = adm_clients.id")
                    .where("adm_clients.email = :email", {email: dataLogin.email}).getRawOne();
      if (!user) {
        return {
          state: 'faild',
          message: 'There is not registered account with the email address that you have entered'
        }
      }
      if (!user.is_active) {
        return {
          state: 'faild',
          message: 'Inactive account'
        }
      }

      // validar la contraseña
      try {
        if (await Tools.comparePassword(dataLogin.password, user.password)) {
          // actualizar el token
          const theToken = await this.library.getRandom(6);
          await getConnection().createQueryBuilder()
                .update(adm_clients).set({token: theToken}).execute();

          
          return {
            state: 'ok',  
            message: 'Welcome to kapturela',
            user_data: {
              is_business: user.is_business,
              email: user.email,
              country: user.country_id,
              account: user.account,
              user_id: user.id,
              password_withdraw: await this.getCheckPasswordWithdraw(user.account),
              token: theToken,
            },
          };
        } else {
          return {
            state: 'faild',
            message: 'The password entered is incorrect, verify and try again'
          }
        }
      }catch(errr) {
        console.log('error');
      }
      
    }
    private async getCheckPasswordWithdraw(userAcccount: string): Promise<boolean> {
      const data = await getConnection().createQueryBuilder()
                    .from(wal_settings_account, "")
                    .where("wal_settings_account.account_id = :account", {account: userAcccount}).getRawOne();

      if (data) {
        return true;
      } else {
        return false;
      }
      
    }
    public async sendMailRecoveryPassowrd(userEmail: string): Promise<any> {
      const respValidateEmail = await this.validateExistsEmail(userEmail);
      if (respValidateEmail.state === 'ok') {
        return {
          state: 'faild',
          message: 'The e-mail provided does not exist'
        }
      }

      const _token = await this.library.getRandom(8);
      try {
        await getConnection().createQueryBuilder()
            .update(adm_clients)
            .set({
              password: await Tools.makePassword(_token),
            })
            .where("email = :email", {email: userEmail}).execute();

        await serviceMail.send('recovery_password', userEmail, 'Recovery Password to Kapture.la', {
          code: _token,
          domain: ENVIRONMENT.DOMAIN
        });

        return {
          state: 'ok',
          message: 'An email was sent with instructions to update your password'
        }
      } catch(err) {
        return {
          state: 'faild',
          message: err.message
        }
      }
      
    }
    public async makeChangePasssword(userToken: string, userPass: string): Promise<any> {
      // validar el token
      const data = await getConnection().createQueryBuilder().
                    from(adm_clients, "adm_clients")
                    .where("token = :token", {token: userToken})
                    .getRawOne();

      if (!data) {
        return {
          state: 'faild',
          message: 'Invalid operation, please request password change again'
        };
      }
      await getConnection().createQueryBuilder().
              update(adm_clients)
              .set({
                password: await Tools.makePassword(userPass),
                token: '',
              }).execute();
      return {
        state: 'ok',
        message:'Password change was successful'
      };
    }
}