import { Request, Response, Router } from "express";
import { adm_clients } from "./models/Clients";
import { SettingsService } from "./settings.service";
import { HttpStatus } from "../../lib/http-status";
import { AuthLogin } from "./auth.interface";
import { AuthMiddleware } from "../../middleware/token.auth";

export class SettingsController extends SettingsService{
    public routers(): any {
        const api = Router();
        
        api.post('/signup', (req: Request, res: Response) => {
            this.setSignup(req.body, res); 
        });

        api.get('/countries', (req: Request, res: Response) => {
            this.getCountries().then(data => {
                return res.status(HttpStatus.OK).json(data);
            })
        });
        api.post('/login', (req: Request, res: Response) => {
            this.setLogin(req.body, res);
        });

        api.post('/recovery-password', (req: Request, res: Response) => {
            this.sendMailRecoveryPassowrd(req.body.email).then(data => {
                if (data.state === 'faild') {
                    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(data);
                } else {
                    return res.status(HttpStatus.OK).json(data);
                }
            }).catch(err => {
                return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
            })

        });

        api.post('/change-password', (req: Request, res: Response) => {
            console.log(req.body);
            this.makeChangePasssword(req.body.token, req.body.pass).then(data => {
                if (data.state === 'faild') {
                    return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(data);
                } else {
                    return res.status(HttpStatus.OK).json(data);
                }
            }).catch(err => {
                return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
            });
        });
        

        return api;
    }

    private async setLogin(data: AuthLogin, res: Response) {
        this.makeLogin(data).then(resp => {
            if (resp.state === 'ok') {
                return res.status(HttpStatus.OK).json(resp);
            } else {
                return res.status(HttpStatus.FORBIDDEN).json(resp);
            }
        }).catch(errr => {
            //console.log(errr);
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(errr);
        })
    }
    private async setSignup(user: adm_clients, res: Response) {
        const result = await this.validateExistsEmail(user.email);
        if (result.state === 'faild') {
            return res.status(HttpStatus.OK).json({
                message: result.message,
                state: 'faild',
            });
        }
        this.setCreateUser(user).then(() => {
            return res.status(HttpStatus.CREATED).json({
                message: 'Welcome has been successfully registered. An email was sent with the account activation code.',
                state: 'ok'
            });
        }).catch(err => {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err);
        });
    }
}