import {Entity, PrimaryColumn, OneToMany} from "typeorm";

@Entity()
export class adm_countries {
    @PrimaryColumn()
    id: number;

    code_country: string;
    
    name_country: string;
}