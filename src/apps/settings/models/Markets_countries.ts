import {Entity, PrimaryColumn, ManyToOne, JoinColumn} from "typeorm";
import { adm_countries } from "./Countries";
import { Length } from "class-validator";
@Entity()
export class adm_markets_countries {
    @PrimaryColumn()
    id: number;

    @Length(3, 3)
    code_currency: string;

    @Length(3, 80)
    currency: string;

    @ManyToOne(type => adm_countries)
    @JoinColumn({ name: "country_id" })
    country: adm_countries;

    active: boolean;
}