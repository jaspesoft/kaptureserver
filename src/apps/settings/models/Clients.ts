import {Entity, PrimaryColumn, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, JoinColumn} from "typeorm";
import { MaxLength, IsEmail, MinLength } from "class-validator";
import { adm_countries } from "./Countries";

@Entity()
export class adm_clients {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column({
        type: "character varying",
        length: 100,
    })
    nro_mobile: string;

    @IsEmail()
    @Column({
        type: "character varying",
        length: 100,
        unique: true,
    })
    email: string;
    
    @Column("character varying")
    @MinLength(8)
    @MaxLength(100)
    password: string;

    @Column("character varying")
    token: string;

    @Column({
        type: "boolean",
        default: false
    })
    is_business: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    is_verified: boolean;

    @Column({
        type: "boolean",
        default: false
    })
    is_active: boolean;

    @Column({ type: 'timestamp with time zone', nullable: false })
    date_joined: Date;

    @ManyToOne(type => adm_countries)
    @JoinColumn({ name: "country_id" })
    country_id = adm_countries;
}