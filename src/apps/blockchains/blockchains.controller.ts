import { Router, Request, Response } from "express";
import { HttpStatus } from "../../lib/http-status";
import { AuthMiddleware } from "../../middleware/token.auth";
import { getConnection } from "typeorm";
import { cy_cryptos } from "../blockchains/models/Cryptos";
import { cy_crypto_prices } from "../prices/models/Crypto_prices";
import { adm_countries } from "../settings/models/Countries";

export class BlockchainsController {
    public routers() {
        const api = Router();

        /*api.get('/prices/:coin/:country', (req: Request, res: Response) => {
            this.getPricesCrypto(req.body, res, req.params.coin);
        });*/

        api.get('/cryptos/:country', (req: Request, res: Response) => {
            this.getAllCrypto(req.body, res, req.params.country);
        });

        return api;
    }

    private async getAllCrypto(req: Request, res: Response, country: string, ) {
        const cryptos = await getConnection()
                            .createQueryBuilder()
                            .select('DISTINCT ON (cy_cryptos.symbol) cy_cryptos.symbol', 'symbol')
                            .addSelect("name_crypto", "")
                            .addSelect("fiat", "")
                            .addSelect("usd", "")
                            .addSelect("sat", "")
                            .innerJoin(cy_crypto_prices, "cy_crypto_prices", "cy_crypto_prices.crypto_id = cy_cryptos.symbol")
                            .from(cy_cryptos, "cy_cryptos")
                            .where("cy_crypto_prices.country_id = :country_id", {country_id: country})
                            .orderBy('symbol')
                            .addOrderBy('date', 'DESC')
                            .getRawMany();
        
        
        return res.status(HttpStatus.OK).json(cryptos);  
    }
}