import ENVIRONMENT from '../../config/environment';
import { BlockchainServices } from '../../shared/services/blockchain.services';
const WebSocket = require('ws');


export default  function BlockchainClientSocket() {
    
    const client = new WebSocket(ENVIRONMENT.SOCKET_BLOCKCHAIN + 'ws/blockchain/');
    client.on('message', (data: any) => {
        data = JSON.parse(data);
        BlockchainServices.registerExistingDeposits(data.txs, data.nro_block);
    });
}