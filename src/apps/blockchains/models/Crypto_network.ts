import {Entity, PrimaryColumn, JoinColumn, ManyToOne} from "typeorm";
import { cy_cryptos } from "./Cryptos";

@Entity()
export class cy_crypto_network {
    @PrimaryColumn()
    id: number;
    
    protocol: string;
    host_rpc: string;
    user_rpc: string;
    pass_rpc: string;
    port_rpc: number;

    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;
}