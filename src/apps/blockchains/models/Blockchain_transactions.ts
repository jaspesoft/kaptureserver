import { cy_blockchains } from "./Blockchain";
import { ManyToOne, PrimaryGeneratedColumn, JoinColumn, Column, Entity } from "typeorm";
import { IsDecimal, MaxLength, IsBoolean, validate } from "class-validator";


@Entity()
export class cy_blockchain_transactions{
    
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column("character varying")
    @MaxLength(300)
    tx: string;

    @IsDecimal()
    @Column("numeric")
    amount: number;

    @Column("character varying")
    @MaxLength(100)
    address: string;

    @Column("integer")
    indx: number;

    @IsBoolean()
    @Column("boolean")
    confirmation: boolean;

    @ManyToOne(type => cy_blockchains)
    @JoinColumn({ name: "nro_block_id" })
    nro_block_id: number;

    
}