import {Entity, PrimaryGeneratedColumn, JoinColumn, ManyToOne, Column, PrimaryColumn, AfterInsert} from "typeorm";
import { cy_cryptos } from "./Cryptos";
import { IsDate, IsDecimal, IsBoolean,MaxLength } from "class-validator";

@Entity()
export class cy_blockchains {
    
    @PrimaryColumn()
    nro_block: number;

    @Column('character varying')
    @MaxLength(300)
    block_hash: string;

    @Column("integer")
    block_size: number;
    
    @IsDate()
    @Column('timestamp with time zone')
    date: Date;

    @IsBoolean()
    @Column("boolean")
    confirmation: boolean;
    
    @ManyToOne(type => cy_cryptos)
    @JoinColumn({ name: "crypto_id" })
    crypto_id: cy_cryptos;

}