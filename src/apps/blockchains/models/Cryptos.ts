import {Entity, PrimaryColumn, OneToMany, Column} from "typeorm";
import { cy_crypto_prices } from "../../prices/models/Crypto_prices";
import { cy_crypto_network } from "./Crypto_network";
import { IsDate, IsDecimal, IsBoolean } from "class-validator";

@Entity()
export class cy_cryptos {
    @PrimaryColumn()
    symbol: string;
    
    name_crypto: string;

    @Column({ type: "integer", default: 1 })
    total_blocks: number;
    
    @Column({ type: "integer", default: 1 })
    sync_blocks: number;

    @Column({ type: "integer"})
    time_block: number;
    
    endpoint_prices: string;
    active: boolean;

    /*@OneToMany(type => CryptoPrices, crypto_prices => crypto_prices.crypto_id)
    crypto_prices: CryptoPrices[];

    @OneToMany(type => CryptoNetwork, crypto_network => crypto_network.crypto_id)
    crypto_network: CryptoNetwork[];*/
    
}