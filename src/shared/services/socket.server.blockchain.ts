const socketio = require('socket.io');
import { BlockchainServices } from './blockchain.services';
import { getConnection } from "typeorm";
import { cy_cryptos } from '../../apps/blockchains/models/Cryptos';
const { forEach } = require('p-iteration');

export class SocketServerBlockchain {
    private sw: any;

    constructor(server: any) { 
        this.sw = socketio(server);
        this.sw.on('connect', () => {
            console.log('cliente conectado');
        });
    }

    public notifySocketDeposit(deposit: any[], crypto: string)  {
        for (const iterator of deposit) {
            this.sw.emit('balance/' + iterator.account, {
                balance: iterator.balance,
                amount: iterator.iterator,
                coin: crypto,
                type: 'deposit',
            });
        }
    }

    public async notifySocketNewTransaction() {
        let crypto_all = await getConnection()
                        .createQueryBuilder()
                        .from(cy_cryptos, "")
                        .where("cy_cryptos.active = :active", {active: true}).getRawMany();

        await forEach(crypto_all, async (crypto: any) => {
            const blockchain = await new BlockchainServices(crypto.symbol);
            const result = await blockchain.getNewTransaction();
            
            for (const iterator of result) {
                this.sw.emit('tx-wait/' + iterator.account, iterator);
            }
        });
    }

    public notifySocketBalance(account: string, balance: number, crypto: string) {
        this.sw.emit('balance/' + account, {
            balance: balance,
            coin: crypto,
            type: 'withdrawal',
        });
    }


}

