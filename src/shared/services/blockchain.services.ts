const core = require("bitcoin-rpc-promise");
const {forEach} = require('p-iteration');
import { getConnection, getManager, QueryRunner } from "typeorm";
import { cy_crypto_network } from "../../apps/blockchains/models/Crypto_network";
import { cy_blockchain_transactions } from "../../apps/blockchains/models/Blockchain_transactions";
import { validate } from "class-validator";
import { wal_wallets } from "../../apps/wallets/models/Wallets";
import { wal_transactions } from "../../apps/wallets/models/Transactions";
import { wal_unspent_addresses } from "../../apps/wallets/models/Unspent_addresses";
import { WalletProvider } from "../providers/wallet.provider";
import { wal_accounts } from "../../apps/wallets/models/Accounts";
import { dbconnection, socketBlockchain } from "../../server";
import { wal_unused_address } from "../../apps/wallets/models/Unuset_addresses";

export class BlockchainServices {
    private coin: string;
    private nodo: any;
    private static tx_no_confirmed: wal_transactions;
    public static walletProvider = new WalletProvider();

    constructor(nodoCoin: string, numberNodo = 0) {
        this.coin = nodoCoin;
    }

    public async getConnection(): Promise<any> {
        //let data = await CoinsModel.findOne({symbol: this.coin}).lean(false).exec();

        let data = await dbconnection
            .createQueryBuilder()
            .from(cy_crypto_network, "")
            .where("cy_crypto_network.crypto_id = :symbol", {symbol: this.coin}).getRawOne();

        if (data !== undefined) {
            return new core(data.protocol + '://' + data.user_rpc + ':' + data.pass_rpc + '@' + data.host_rpc + ':' + data.port_rpc);
        }

    }


    public static async registerExistingDeposits(txArray: any[], nroBlock: number) {
        await forEach(txArray, async (txid: any) => {

            const queryRunner = getConnection().createQueryRunner();
            await queryRunner.connect();

            await queryRunner.startTransaction();

            try {
                const obj_tx = await dbconnection.createQueryBuilder()
                    .from(cy_blockchain_transactions, "")
                    .where("cy_blockchain_transactions.tx = :tx", {
                        tx: txid
                    })
                    .getRawOne();

                if (!await BlockchainServices.checkDeposit(obj_tx, queryRunner)) {
                    queryRunner.rollbackTransaction();
                } else {
                    queryRunner.commitTransaction();
                }

            } catch (err) {
                queryRunner.rollbackTransaction();
            } finally {
                queryRunner.release();
            }

        });
    }

    private static async checkDeposit(obj_tx: cy_blockchain_transactions, runner: QueryRunner): Promise<boolean> {
        let wallet: any;

        // verificar si la address ya tiene un deposito registrado como no confirmado
        if (!await BlockchainServices.checkAddressRegisterNotConfirmed(obj_tx)) {
            // busco si el address que se acaba de registrar corresponde a una address que esta por recibir pagos

            wallet = await BlockchainServices.checkAddressExistsPlataform(obj_tx);
            if (!wallet) {
                return true;
            }
            return await BlockchainServices.saveTransaction(obj_tx, wallet, runner);
        } else {
            return await BlockchainServices.updateTransaction(obj_tx, this.tx_no_confirmed, runner);
        }

    }

    private static async updateTransaction(obj_tx: cy_blockchain_transactions, wallet: any, runner: QueryRunner): Promise<boolean> {
        const current_child = wallet.child;
        const new_unspent = new wal_unspent_addresses();

        new_unspent.address = obj_tx.address;
        new_unspent.amount = obj_tx.amount;
        new_unspent.child = current_child;
        new_unspent.tx = obj_tx.tx;
        new_unspent.txindx = obj_tx.indx;
        new_unspent.wallet_id = wallet.id;
        new_unspent.change = wallet.change;
        new_unspent.is_spent = false;

        await runner.manager.save(new_unspent);
        await runner.manager.createQueryBuilder()
            .update(wal_transactions)
            .set({
                confirmed: true
            }).where("wal_transactions.address  = :address and wal_transactions.tx = :tx and wal_transactions.confirmed = :confirmed and wal_transactions.amount = :amount", {
                address: obj_tx.address,
                tx: obj_tx.tx,
                amount: obj_tx.amount,
                confirmed: false,
            }).execute();

        // suma de todas las direcciones no gastadas para actualizar el balance

        const new_balance = await runner.manager.createQueryBuilder()
            .select("COALESCE( SUM(wal_unspent_addresses.amount), 0)", "sum")
            .from(wal_unspent_addresses, "wal_unspent_addresses")
            .where("wal_unspent_addresses.is_spent = :spent", {spent: false})
            .andWhere("wal_unspent_addresses.wallet_id = :wallet", {wallet: wallet.id})
            .getRawOne();


        // buscar datos de la cuenta para derivar una nueva direccion de pago
        const account = await runner.manager.createQueryBuilder()
            .from(wal_accounts, "")
            .where("wal_accounts.id = :accountId ", {accountId: wallet.account_id})
            .getRawOne();

        // actualiza a wallet con el hijo y direccion nueva para recibir futuros depositos
        const childWallet = current_child + 1;
        const wallet_blockchain = await this.walletProvider.getWallet(wallet.crypto_id, account.seed, String(childWallet));
        await runner.manager.createQueryBuilder()
            .update(wal_wallets)
            .set({
                address: wallet_blockchain.address,
                child: childWallet,
                balance: Number(new_balance.sum)
            }).where("id = :idWallet", {idWallet: wallet.id})
            .execute();

        return true;

    }

    private static async saveTransaction(obj_tx: cy_blockchain_transactions, wallet: any, runner: QueryRunner): Promise<boolean> {
        const current_child = wallet.child;

        const new_unspent = new wal_unspent_addresses();

        new_unspent.address = obj_tx.address;
        new_unspent.amount = obj_tx.amount;
        new_unspent.child = current_child;
        new_unspent.change = wallet.change;
        new_unspent.tx = obj_tx.tx;
        new_unspent.txindx = obj_tx.indx;
        new_unspent.wallet_id = wallet.id;
        new_unspent.change = wallet.change;
        new_unspent.is_spent = false;

        // guardar la transacción de deposito

        const historial = new wal_transactions;
        historial.address = obj_tx.address;
        historial.amount = obj_tx.amount;
        historial.tx = obj_tx.tx;
        historial.type_transaction = 'C';
        historial.crypto_id = wallet.crypto_id;
        historial.wallet_id = wallet.id;
        historial.confirmed = true;
        historial.date = new Date();

        await runner.manager.save(new_unspent);
        await runner.manager.save(historial);

        // suma de todas las direcciones no gastadas para actualizar el balance
        const new_balance = await runner.manager.createQueryBuilder()
            .select("COALESCE( SUM(wal_unspent_addresses.amount), 0)", "sum")
            .from(wal_unspent_addresses, "wal_unspent_addresses")
            .where("wal_unspent_addresses.is_spent = :spent", {spent: false})
            .andWhere("wal_unspent_addresses.wallet_id = :wallet", {wallet: wallet.id})
            .getRawOne();

        // buscar datos de la cuenta para derivar una nueva direccion de pago
        const account = await runner.manager.createQueryBuilder()
            .from(wal_accounts, "")
            .where("wal_accounts.id = :accountId ", {accountId: wallet.account_id})
            .getRawOne();

        // actualiza a wallet con el hijo y direccion nueva para recibir futuros depositos
        const childWallet = current_child + 1;
        const wallet_blockchain = await this.walletProvider.getWallet(wallet.crypto_id, account.seed, String(childWallet));

        await runner.manager.createQueryBuilder()
            .update(wal_wallets)
            .set({
                address: wallet_blockchain.address,
                child: childWallet,
                balance: Number(new_balance.sum),
            }).where("id = :idWallet", {idWallet: wallet.id})
            .execute();

        socketBlockchain.notifySocketDeposit([{
            account: wallet.account_id,
            amount: obj_tx.amount,
            balance: new_balance.sum
        }], wallet.crypto_id);

        return true;

    }

    /**
     * Verifica si el address que recibe las criptos ya se encuentra registrada en la db como no confirmado
     */
    private static async checkAddressRegisterNotConfirmed(obj_tx: cy_blockchain_transactions): Promise<boolean> {
        const no_confirmed = await dbconnection.createQueryBuilder()
            .select("wal_transactions.address", "address")
            .addSelect("wal_transactions.amount", "amount")
            .addSelect("wal_transactions.tx", "tx")
            .addSelect("wal_wallets.balance", "balance")
            .addSelect("wal_wallets.crypto_id", "crypto_id")
            .addSelect("wal_wallets.account_id", "account_id")
            .addSelect("wal_transactions.child", "child")
            .addSelect("wal_transactions.change", "change")
            .addSelect("wal_wallets.id", "id")
            .innerJoin(wal_wallets, "wal_wallets", "wal_wallets.id = wal_transactions.wallet_id")
            .from(wal_transactions, "")
            .where("wal_transactions.address  = :address and wal_transactions.tx = :tx and wal_transactions.confirmed = :confirmed and wal_transactions.amount = :amount", {
                address: obj_tx.address,
                tx: obj_tx.tx,
                amount: obj_tx.amount,
                confirmed: false,
            }).getRawOne();
        if (no_confirmed) {
            this.tx_no_confirmed = no_confirmed;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checa si el address de la blockchain pertenece algun cliente en la plataforma
     */
    private static async checkAddressExistsPlataform(obj_tx: cy_blockchain_transactions): Promise<wal_wallets> {
        let data = await dbconnection.createQueryBuilder()
            .from(wal_wallets, "wal_wallets")
            .where("wal_wallets.address = :address ", {address: obj_tx.address})
            .getRawOne();
        if (data) {
            return data;
        }
        return await dbconnection.createQueryBuilder()
            .select("wal_unspent_addresses.child", "child")
            .addSelect("wal_unspent_addresses.change", "change")
            .addSelect("wal_wallets.id", "id")
            .addSelect("wal_wallets.crypto_id", "crypto_id")
            .addSelect("wal_wallets.account_id", "account_id")
            .innerJoin(wal_wallets, "wal_wallets", "wal_wallets.id = wal_unspent_addresses.wallet_id")
            .from(wal_unspent_addresses, "")
            .where("wal_unspent_addresses.address = :address", {address: obj_tx.address})
            .getRawOne();

    }

    public async createArrayTx(txid: string, txinfo: any): Promise<any[]> {
        let x: any = [];

        await forEach(txinfo.vout, async (vout: any) => {
            if (vout.scriptPubKey.type === 'pubkey' || vout.scriptPubKey.type === 'pubkeyhash') {
                x.push({
                    tx: txid,
                    amount: vout.value,
                    addresses: vout.scriptPubKey.addresses[0],
                    indx: vout.n,
                });

            } else {
                x.push({
                    tx: txid,
                    amount: vout.value,
                    addresses: 'NONE',
                    indx: vout.n,
                });
            }
        });

        return x;
    }

    async getNewTransaction(): Promise<any[]> {
        this.nodo = await this.getConnection();
        let result = [];
        try {
            result = await this.nodo.getRawMempool();
        } catch (err) {
            return [];
        }

        if (result.length === 0) {
            return [];
        }

        let tx_result: any = [];
        await forEach(result, async (tx_hash: any) => {

            const decode_tx = await this.nodo.decoderawtransaction(await this.nodo.getrawtransaction(tx_hash));
            const array_tx = await this.createArrayTx(tx_hash, decode_tx);
            tx_result.push(array_tx);

        });

        let notify: any = [];
        await forEach(tx_result, async (iterator: any) => {

            for (const obj of iterator) {
                const data = await dbconnection.getRepository(wal_unused_address)
                    .findOne({where: {address: obj.addresses}})

                if (!data) {
                    continue;
                }
                try {
                    // guardar la transaccion de tipo deposito
                    const tx_pg = new wal_transactions();
                    tx_pg.address = obj.addresses;
                    tx_pg.amount = obj.amount;
                    tx_pg.confirmed = false;
                    tx_pg.crypto_id = data.crypto_id;
                    tx_pg.date = new Date();
                    tx_pg.tx = obj.tx;
                    tx_pg.type_transaction = 'C';
                    tx_pg.wallet_id = data.wallet_id;
                    tx_pg.change = data.change;
                    tx_pg.child = data.child;

                    await validate(tx_pg);
                    await getManager().save(tx_pg);

                    notify.push({
                        amount: obj.amount,
                        tx: obj.tx,
                        account: data.wallet_id.account_id.id,
                        address: tx_pg.address,
                        type_transaction: 'C',
                        confirmed: false,
                        date: tx_pg.date,
                        crypto: data.crypto_id
                    })
                } catch (e) { }

            }
        });

        return notify;
    }
}
