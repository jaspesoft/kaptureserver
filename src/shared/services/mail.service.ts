import nodemailer from "nodemailer";
const hbs =  require('nodemailer-express-handlebars');

class Mail {
    private transport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: 'kapturela@gmail.com',
            pass: '130202angel'
        },
    });
    private ViewOption: any;
    constructor() {
        const handlebarOptions = {
            viewEngine: {
                extName: '.hbs',
                partialsDir: '../../templates/email',
                layoutsDir: '../../templates/email',
                defaultLayout: '',
              },
              viewPath: __dirname + '/../../templates/email',
              extName: '.hbs',
        };
        this.transport.use('compile', hbs(handlebarOptions));
    }

    public async send(templateName: string, to_email: string, to_subject: string, templateContext: any): Promise<any> {
        const HelperOptions = {
            from: 'Contact <kapturela@gmail.com>',
            to: to_email,
            subject: to_subject,
            template: templateName,
            context: templateContext
        };
        this.transport.sendMail(HelperOptions);
    }
}

export const serviceMail = new Mail();
