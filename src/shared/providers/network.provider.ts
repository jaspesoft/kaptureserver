import { networks } from "bitcoinjs-lib";


export class NetworkProvider {

    private code: number;
    private testnet: boolean;

    public getTypeNetwork() {
        return this.testnet;
    }
    public getCodeCrypto() {
        return this.code + '';
    }
    public getNetworkParams(symbol: string): any {
        //networks.bitcoin.messagePrefix = '\x19kapturela Signed Message:\n';
        // pubKeyHash=== PUBKEY_ADDRESS y SCRIPT_ADDRESS = scriptHash wif= HEX( PUBKEY_ADDRESS + 128 )
        switch (symbol) {
            case 'ONX':
                this.code = 174;
                networks.bitcoin.bip32.private = 0x0488ade4;
                networks.bitcoin.bip32.public = 0x0488b21e;
                networks.bitcoin.pubKeyHash = 0x4B;
                networks.bitcoin.scriptHash = 0x05;
                networks.bitcoin.wif = 0xCB;
                break;
            case 'LKR':
                this.code = 557;
                networks.bitcoin.messagePrefix = 'LakrassCoin Signed Message:\n';
                networks.bitcoin.bip32.private = 0x0488ade4;
                networks.bitcoin.bip32.public = 0x0488b21e;
                networks.bitcoin.pubKeyHash = 0x30;
                networks.bitcoin.scriptHash = 0x55;
                networks.bitcoin.wif = 0xB0;
                break;

            case 'BOLI':
                this.code = 99;
                networks.bitcoin.bip32.private = 0x0488ade4;
                networks.bitcoin.bip32.public = 0x0488b21e;
                networks.bitcoin.pubKeyHash = 0x55;
                networks.bitcoin.scriptHash = 0x05;
                networks.bitcoin.wif = 0xD5;
                break;
                
            case 'DOGE':
                networks.bitcoin.bip32.private = 0x02fac398;
                networks.bitcoin.bip32.public = 0x02facafd;
                networks.bitcoin.pubKeyHash = 0x1e;
                networks.bitcoin.scriptHash = 0x16;
                networks.bitcoin.wif = 0x9e;
                break;

            case 'RIL':
                this.code = 99;
                networks.bitcoin.bip32.private = 0x0488ADE4;
                networks.bitcoin.bip32.public = 0x0488B21E;
                networks.bitcoin.pubKeyHash = 49;
                networks.bitcoin.scriptHash = 5;
                networks.bitcoin.wif = 177;
                break;

            case 'ARION':
                this.code = 99;
                networks.bitcoin.bip32.private = 0x0488ADE4;
                networks.bitcoin.bip32.public = 0x0488B21E;
                networks.bitcoin.pubKeyHash = 23;
                networks.bitcoin.scriptHash = 15;
                networks.bitcoin.wif = 151;
                break;

            default:
                this.code = 1;
                break;
            
        }
      
        if (this.testnet) {
            return networks.testnet;
        } else {
           return networks.bitcoin;
        }
    }
}