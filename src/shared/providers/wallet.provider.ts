import { NetworkProvider } from "./network.provider";
//import { paramsBlockchain } from "../../settings";
import { payments } from 'bitcoinjs-lib';
import {  generateMnemonic, mnemonicToSeed  } from 'bip39';
import { fromSeed, fromBase58 } from 'bip32';
//import Base64 from "js-base64";
import QRCode from 'qrcode';

export class WalletProvider {
    public crypto_network = new NetworkProvider();
    constructor() { }

    public static getCreateMnemonic(): string {
        //return WalletProvider.getEncrypt(generateMnemonic());
        return generateMnemonic();
    }

    private getPath(code_crypto: string, child: string, is_change = false, is_address = true, bitp44 = false) {
        if (bitp44) {
            if (is_change) {
                return 'm/44\'/' + code_crypto + '\'/0\'' + '\/1\'/' + child + '';
            } else {
                if (is_address) {
                    return 'm/44\'/' + code_crypto + '\'/0\'' + '/0/' + child + '';
                } else {
                    return 'm/44\'/' + code_crypto + '\'/' + child + '\'';
                }
            }
        } else {
            //return 'm/0\'/' + code_crypto + '\'/0\'' + '\/1\'/' + child + '';
            if (is_change) {
                return "m/0'/1/" + child; 
            } else {
                return "m/0'/0/" + child;
            }
        }
        
    }

    private async getSeed(mnemonic: string, crypto: string) {
        const seed = await mnemonicToSeed(mnemonic);
        return fromSeed(seed, this.crypto_network.getNetworkParams(crypto));
    }

    public getMasterKey(crypto: string, mnemonic: string) {
        return this.getSeed(mnemonic, crypto);
    }

    public async getWallet(crypto: string, mnemonic: string, nroChild = '0', childChange = false) {
        let master = null;
        let xprvString = null;

        //(async ()  => {
        master = await this.getMasterKey(crypto, mnemonic);
        xprvString =  await master.toBase58();    
        //})();

        const network = this.crypto_network.getNetworkParams(crypto);
        const xpubString = fromBase58(xprvString, network).derivePath(
            this.getPath(this.crypto_network.getCodeCrypto(), nroChild, childChange, false),
            ).neutered().toBase58();

        const ch = this.getPaymentAddress(master, crypto, nroChild, childChange);

        return {
            xpriv: xprvString,
            xpub: xpubString,
            child: nroChild,
            address: ch.address,
            prv: ch.prv,
            testnet: this.crypto_network.getTypeNetwork(),
            qr: await this.getQRAddress(ch.address)
        };
    }

    public async getQRAddress(address: string): Promise<any> {
        return await QRCode.toDataURL(address);
    }

    public getPaymentAddress(xpriv: any, crypto: string, child: string, is_change = false) {
        const x = xpriv.derivePath( this.getPath(
            this.crypto_network.getCodeCrypto(), child, is_change),
        );

        const network = this.crypto_network.getNetworkParams(crypto);

        return {
            address: payments.p2pkh({pubkey: x.publicKey, network}).address,
            prv: x.toWIF(),
        } ;
    }
}
