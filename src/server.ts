import app from "./app";
import { SocketServerBlockchain } from "./shared/services/socket.server.blockchain";
import { pricesService } from "./apps/prices/prices.service";
import { createConnection } from "typeorm";
import cron from "node-cron";
import fs from 'fs';
import https from 'https';
import ENVIRONMENT from "./config/environment";
import { Connection } from "typeorm/connection/Connection";

let socket: SocketServerBlockchain;
let connection: Connection;

try {
    (async () => {
        connection = await createConnection();
        let server: any;
        if (!ENVIRONMENT.DEV) {
            const credentials = {
                key: fs.readFileSync('/etc/nginx/ssl/kapture_la.key', 'utf-8'),
                cert: fs.readFileSync('/etc/nginx/ssl/kapture_la.crt', 'utf-8')
            };
            server = https.createServer(credentials, app).listen(app.get("port"));

        } else {
            server = app.listen(app.get("port"), () => {
                console.log(
                    "  App is running at http://localhost:%d in %s mode",
                    app.get("port"),
                    app.get("env")
                );
                console.log("  Press CTRL-C to stop\n");
            });
        }

        socket = new SocketServerBlockchain(server);

        cron.schedule('*  *  *  *  *', () => {
            socket.notifySocketNewTransaction();
        });

        cron.schedule('*/20  *  *  *  *', () => {
            pricesService.taskPrices();
        });
    })();
} catch (e) {
    console.log("TypeORM connection error: ", e);
}

export const socketBlockchain = socket;
export const dbconnection = connection;
