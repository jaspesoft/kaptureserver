 import  bcrypt = require('bcrypt');

export class Tools {
    public async getRandom(length: number): Promise<string> {
        let result: string;
        const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        for (let i = length; i > 0; --i) {
          result = result + chars[Math.floor(Math.random() * chars.length)];
        }
        return result.replace('undefined', '');
    }
    public static async comparePassword(pass: string, hash: string): Promise<boolean> {
        return await bcrypt.compare(pass, hash);
    }
    public static makePassword(pass: string) {
        return  bcrypt.hash(pass, 10);
    }
}