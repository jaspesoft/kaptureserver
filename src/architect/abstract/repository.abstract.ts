import { ObjectLiteral, Repository } from "typeorm";
import { wal_unspent_addresses } from "../../apps/wallets/models/Unspent_addresses";

export abstract class RepositoryAbstract {
    abstract getClassRepository(): Repository<any>;

    async delete(params: object) {
        await this.getClassRepository().delete(params);
    }

    async updateOne(id: number, fieldsUpdate: object) {
        await this.getClassRepository().update(id, fieldsUpdate);
    }

    async save(entity: ObjectLiteral) {
        await this.getClassRepository().save(entity);
    }
}
