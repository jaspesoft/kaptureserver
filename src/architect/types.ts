export const TYPES = {
    Wallet: Symbol("WalletRepository"),
    Unspent: Symbol("UnspentAddress"),
};
