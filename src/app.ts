import express from "express";
import bodyParser = require("body-parser");
import ENVIRONMENT from "./config/environment";
import { BlockchainsController } from "./apps/blockchains/blockchains.controller";
import { SettingsController } from "./apps/settings/settings.controller";
import { WalletsController } from "./apps/wallets/wallets.controller";
import { PricesController } from "./apps/prices/prices.controller";
import BlockchainClientSocket from './apps/blockchains/blockchain.client.socket';

const cors = require("cors");
const rateLimit = require("express-rate-limit");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors({
    "origin": "*",
    "methods": "GET, HEAD, PUT, POST, DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
}));

app.disable('x-powered-by');

app.use(
    new rateLimit({
        windowMs: 15 * 60 * 1000,
        max: 100,
    }),
);
app.set("port", ENVIRONMENT.APP_PORT);

export default app;


BlockchainClientSocket();

app.use("/api/v1/blockchain", new BlockchainsController().routers());
app.use("/api/v1/auth", new SettingsController().routers());
app.use("/api/v1/wallets", new WalletsController().routers());
app.use("/api/v1/prices", new PricesController().routers());
